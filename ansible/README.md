= Documentation of Vfoss Infrastructure

== Webserver Setup

== Database Setup
This role https://galaxy.ansible.com/amnay-mo/postgresql/ is used to provision postgres production cluster & replication.
Modified configurations are used to provision dev machine, which is documented below.

=== Development Machine Setup
Development machine become postgressql master.

playbook: 	vfoss_dev_postgres_setup_playbook.yml
hosts: 			vfoss_dev
vars:				vars_vfoss_dev.yml 
group_vars:	vfoss_dev


= Ansible Quick Start & Development

== Set up SSH-agent so it can remember our credentials:

$ ssh-agent bash
$ ssh-add ~/.ssh/id_rsa

== Run command:
$ ansible all -m ping -u remote_user
$ ansible atlanta -a "/usr/bin/foo" -u username --become-user otheruser [--ask-become-pass]

The command - Executes a command on a remote node module does not support shell variables and things like piping. If we want to execute a module using a shell, use the ‘shell’ module instead. Read more about the differences on the About Modules page.

=== Using the shell - Execute commands in nodes. module looks like this:

$ ansible raleigh -m shell -a 'echo $TERM'

== Transfer a file directly to many servers:

$ ansible atlanta -m copy -a "src=/etc/hosts dest=/tmp/hosts"

=== The file module allows changing ownership and permissions on files. These same options can be passed directly to the copy module as well:

$ ansible webservers -m file -a "dest=/srv/foo/a.txt mode=600"
$ ansible webservers -m file -a "dest=/srv/foo/b.txt mode=600 owner=mdehaan group=mdehaan"

The file module can also create directories, similar to mkdir -p:

$ ansible webservers -m file -a "dest=/path/to/c mode=755 owner=mdehaan group=mdehaan state=directory"

As well as delete directories (recursively) and delete files:

$ ansible webservers -m file -a "dest=/path/to/c state=absent"

=== synchronize (rsync)

- name: Fetch stuff from the remote and save to local
  synchronize:  src={{ item }} dest=/tmp/ mode=pull
	  with_items:
		    - "folder/one"
				    - "folder/two"
== Show facts
$ ansible hostname -m setup | less

== Show host variables
Valid variable sources (host_vars, group_vars, _meta in a dynamic inventory, etc.) are all taken into account.

ansible vfoss_dev -m debug -a "var=hostvars[inventory_hostname]"
ansible vfoss_dev -m debug -a "var=vars"
