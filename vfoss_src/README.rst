===============================
anthenao: BUILDING S2I IMAGE
===============================

.. image:: https://badge.fury.io/py/anthenao.png
    :target: http://badge.fury.io/py/anthenao

.. image:: https://pypip.in/d/anthenao/badge.png
    :target: https://crate.io/packages/anthenao?version=latest


www.anthenao.com

* Free software: BSD license

Requirements
------------

* Django 1.8+
* Python 2.7
* Django CMS 3.2+

.. _django-cms: https://github.com/divio/django-cms

Installation
----------------------------

#. Clone the git repository.
#. Create a production.py file in anthenao/anthenao/anthenao/settings by copying what's in the example_production.py. If production.py is not available, dev.py will be used. See __init__.py under settings folder.
    * Fill database details in the file you just created
    * Add the site admins in the ADMINS variable
    * Add server host in ALLOWED_HOSTS

#. Install all third party packages by running::

    $ pip install -r requirements.txt

   currently using k8s.txt packages. 

    $ pip install -r requirements/development.txt

#. Apply migrations::

    $ python manage.py migrate

Docker S2I
----------------------------
TODO: what are these Dockerfiles:
Dockerfile = Dockerfile.production: TODO?
Dockerfile.development: TODO ?
Dockerfile.s2i.base: TODO build s2i image

Docker Compose
----------------------------
Docker compose build only run Dockerfile. S2I image must be built with s2i then run by docker compose (up).

Update Source
----------------------------
s2i build --loglevel=4 anthenao docker_id/image_name anthenao_img_web_final


Development
----------------------------
Use python27 container, mount source and dev. See Dockerfile.development.

'''
sudo docker run -it -u root -v $(pwd)/anthenao:/opt/app-root/src \
   -v $(pwd)/logs:/opt/app-root/logs \
   -p 8000:8080 anthenao_dev_img bash

docker run -it -u root -v $(pwd)/vfoss_org:/opt/app-root/src -v $(pwd)/logs:/opt/app-root/logs -p 8000:8080 vfoss_img_web:dev bash

docker-compose -f docker-compose-dev.yml run --service-ports web bash

#Install packages, make changes

pip install requirements/development.txt

#Run Server avoid binding to 127.0.0.1, can not access from outside container:

python manage.py runserver 0.0.0.0:8080
'''

Note
----------------------------
== Environment

s2i script uses app/.s2i/environment to set ENV.
docker-compose uses 'environment' list in docker-compose.yml or a file under 'env_file'. Point to the s2i file for consistency.
