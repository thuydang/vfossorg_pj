The pages template that is referred by settings.py is in pjs/templates. These pages extend themes templates, which are in pjs/themes, e.g., new_theme. In new_theme/template contains a complete website. The folder new_theme/templates/pages contains all pages, home, 1 col, 2 col, etc. The pages are built with everything inside new_theme/templates. The folder new_theme/custom contains customized pages.

templates/base.html extends new_theme/custom/base.html extends new_theme/pages/base.html

== Old ==
All plugins should extend _extend_theme_proxy.html which points to a full template of choosen theme (new_theme, debug_theme). 
However, this make their template depends on any 3rd party template tags used by the main theme. E.g, removing a template tag 
may cause template error when loading those plugins.

Plugins can extend the _extend_layout.html in the choosen theme for maximum customization. Only the layout is kept.
