from django.conf import settings
from django.urls import include, path
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin

from django.contrib.auth import views as auth_views

#admin.autodiscover() # Not required for Django 1.7.x+

urlpatterns = i18n_patterns(
    path(r'admin/', admin.site.urls),

    ## python-social-auth
    path('accounts/', include(('django.contrib.auth.urls', 'auth'), namespace='auth')),
    #path('', include('django.contrib.auth.urls')),
    #path('social/', include('social.apps.django_app.urls')),
    path('social/', include(('social_django.urls', 'social'), namespace='social')),

    ##
    #path('password_reset_done/', auth_views.password_reset_done, name='password_reset_done'),
    #path('password_change_done/', auth_views.password_change_done, name='password_change_done'),
    #override the default urls
    #path(r'^password/change/$',
    #    auth_views.password_change,
    #    name='password_change'),
    #path(r'^password/change/done/$',
    #    auth_views.password_change_done,
    #    name='password_change_done'),
    #path(r'^password/reset/$',
    #    auth_views.password_reset,
    #    name='password_reset'),
    #path(r'^password/reset/done/$',
    #    auth_views.password_reset_done,
    #    name='password_reset_done'),
    #path(r'^password/reset/complete/$',
    #    auth_views.password_reset_complete,
    #    name='password_reset_complete'),
    #path(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
    #    auth_views.password_reset_confirm,
    #    name='password_reset_confirm'),

    ## djangocms-blog
    path(r'^taggit_autosuggest/', include('taggit_autosuggest.urls')),

    ## vfoss-news
#    path(r'^news/', include('vfoss_news.urls')),

    ## django-cms
    path(r'', include('cms.urls')),
    # hide default language code abc.com/en/ --> abc.com
    prefix_default_language=False
    ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#from django.contrib.auth import views as auth_views
#urlpatterns += [
#    path(r'^change-password/$', 'django.contrib.auth.views.password_change', name="password-change")
#]

#urlpatterns += [
#		# misago
#		path(r'forums^', include('misago.urls')),
#		]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
            path(r'^__debug__/', include(debug_toolbar.urls)),
            ]
