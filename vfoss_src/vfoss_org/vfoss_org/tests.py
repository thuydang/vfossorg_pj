# -*- coding: utf-8 -*-
from django.test import TestCase

import time
import logging

from django.contrib.auth import get_user_model
from people.models import Person


logging.basicConfig(level=logging.DEBUG)

class TestPeopleModel(TestCase):
    log = logging.getLogger('Test People Model')

    def test_profile_creation(self):
        User = get_user_model()
        # New user created
        user = User.objects.create(
                username="taskbuster", password="django-tutorial")
        # Check that a Profile instance has been crated
        self.assertIsInstance(user.profile, Person)

        self.log.debug('Profile.str: ' + str(user.profile) )
        # Call the save method of the user to activate the signal
        # again, and check that it doesn't try to create another
        # profile instace
        user.save()
        self.assertIsInstance(user.profile, Person)
