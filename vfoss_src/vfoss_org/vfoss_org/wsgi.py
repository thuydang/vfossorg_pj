"""
WSGI config for vfoss_org project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "vfoss_org.settings")

from django.core.wsgi import get_wsgi_application
from django.conf import settings

if settings.PRODUCTION:
  from dj_static import Cling, MediaCling
  application = Cling(MediaCling(get_wsgi_application()))
  #application = Cling(get_wsgi_application())
else:
  application = get_wsgi_application()

