"""
Django settings for vfoss_org project.

*** This is TEST ENV ***

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import json
# Normally you should not import ANYTHING from Django directly
# into your settings, but ImproperlyConfigured is an exception.
from django.core.exceptions import ImproperlyConfigured
from django.contrib import auth

from os import environ, getenv
from os.path import abspath, basename, dirname, join, normpath
from sys import path

gettext = lambda s: s

########## PATH CONFIGURATION
# vfoss_org/vfoss_org/
BASE_DIR = os.path.join(dirname(__file__) , "../")

# Absolute filesystem path to the config directory:

#CONFIG_ROOT = os.path.abspath(os.path.dirname(__file__))
CONFIG_ROOT = dirname(dirname(abspath(__file__)))

# Absolute filesystem path to the project directory:
PROJECT_ROOT = os.path.join(dirname(BASE_DIR) , "../")

# Absolute filesystem path to the django repo directory:
DJANGO_ROOT = dirname(PROJECT_ROOT)

# Project name:
PROJECT_NAME = basename(PROJECT_ROOT).capitalize()

# Project folder:
PROJECT_FOLDER = basename(PROJECT_ROOT)

# Project domain:
PROJECT_DOMAIN = '%s.com' % PROJECT_NAME.lower()

# Add our project to our pythonpath, this way we don't need to type our project
# name in our dotted import paths:
path.append(CONFIG_ROOT)
########## END PATH CONFIGURATION



# JSON-based secrets module
with open(os.path.join(dirname(PROJECT_ROOT), "test.env.json")) as f:
    secrets = json.loads(f.read())

def get_secret(setting, secrets=secrets):
    """Get the secret variable or return explicit exception."""
    try:
        return secrets[setting]
    except KeyError:
        error_msg = "Set the {0} environment variable".format(setting)
        raise ImproperlyConfigured(error_msg)


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# Application definition

INSTALLED_APPS = (
    'django.contrib.sites',
    'djangocms_admin_style',  # for the admin skin. You **must** add 'djangocms_admin_style' in the list **before** 'django.contrib.admin'.
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',  # to enable messages framework (see :ref:`Enable messages <enable-messages>`)
    'django.contrib.staticfiles',
    'formtools', # splitted from Django since 1.8 to django-formtools
    'djangocms_text_ckeditor',
    'cms',  # django CMS itself
    #'south',  # Only needed for Django < 1.7
    'sekizai',  # for javascript and css management
    'reversion',
#    #-- django-people --
    'filer',
    'easy_thumbnails',
#    'people', # Instale from customized django-people: https://github.com/vfoss-org/django-people.git
    'treebeard', # replacement for mptt
    #'mptt',  # utilities for implementing a tree
    'menus',  # helper for model independent hierarchical website navigation
    #-- python-social-auth --
    #'social.apps.django_app.default',
    'social_django',
    #-- vfoss-news --
    'vfoss_news',
    'hvad',
    #-- djangocms_blog --
    #'filer', # included
    #'easy_thumbnails', # included # included
    'aldryn_apphooks_config',
#    'cmsplugin_filer_image',
    'parler',
    'taggit',
    'taggit_autosuggest',
    'django_select2',
    'meta',
    'sortedm2m',
    #'admin_enhancer',
    'djangocms_blog',
    #-- aldryn-facebook --
    #'aldryn_facebook',
    #-- aldryn-news --
    #'aldryn_news',
    #'taggit',
    #'aldryn_search',
    #'django_select2',
    ##'djangocms_text_ckeditor',
    #'easy_thumbnails',
    #'filer',
    #'hvad',
    #'haystack', # for search
    #-- aldryn-blog
    #'aldryn_blog',
    #'aldryn_common',
    #'django_select2',
    #'djangocms_text_ckeditor',
    #'easy_thumbnails',
    #'filer',
    #'hvad',
    #'taggit',
    #'hvad',
    # for search
    #'aldryn_search',
    #'haystack',
    #--
    #'debug_toolbar',
    #-- social tools --
    'django_social_share',
    'social_widgets', #django-social-widgets
    #-- analytical tools --
    'analytical',
    )

MIDDLEWARE_CLASSES_178 = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.doc.XViewMiddleware', # for django 1.7.8
    'django.middleware.common.CommonMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    )

MIDDLEWARE_CLASSES_cms30 = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

# CMS 3.4.1
MIDDLEWARE_CLASSES = (
    'django.middleware.security.SecurityMiddleware',
    'cms.middleware.utils.ApphookReloadMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    )

# CMS 3.6.0 Django 2.0
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'cms.middleware.utils.ApphookReloadMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]


###(1_8.W001) The standalone TEMPLATE_* settings were deprecated in Django 1.8 
###and the TEMPLATES dictionary takes precedence. You must put the values of the 
###following settings into your default TEMPLATES dict: TEMPLATE_DIRS, TEMPLATE_CONTEXT_PROCESSORS, TEMPLATE_DEBUG.
###djangocms_blog.Post.sites: (fields.W340) null has no effect on ManyToManyField.

MY_TEMPLATE_DEBUG = True

MY_TEMPLATE_CONTEXT_PROCESSORS = (
        'django.contrib.auth.context_processors.auth',
        'django.contrib.messages.context_processors.messages',
        'django.template.context_processors.debug',
        'django.template.context_processors.i18n',
        'django.template.context_processors.media',
        'django.template.context_processors.static',
        'django.template.context_processors.tz',
        'django.template.context_processors.request',
        #'django.core.context_processors.i18n',
        #'django.core.context_processors.request',
        #'django.core.context_processors.media',
        #'django.core.context_processors.static',
        'sekizai.context_processors.sekizai',
        'cms.context_processors.cms_settings',
        # python-social-auth
        'social_django.context_processors.backends',
        'social_django.context_processors.login_redirect',
        )


MY_TEMPLATE_DIRS = (
        # The docs say it should be absolute path: BASE_DIR is precisely one.
        # Life is wonderful!
        os.path.join(BASE_DIR, "templates"),
        os.path.join(BASE_DIR, "themes"),
        )

CMS_TEMPLATES = (
        ('vfoss_org/base.html', 'Base Template'),
        ('vfoss_org/2cols_sidebar.html', '2 Cols - Sidebar Right'),
        # Debug theme
        #('debug_theme/base.html', 'Debug base'),
        #('debug_theme/template_1.html', 'Debug tpl 1'),
        #('debug_theme/template_2.html', 'Debug tpl 2'),
        )


TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            #'DIRS': [os.path.join(BASE_DIR, "templates"),
            'DIRS': MY_TEMPLATE_DIRS,
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': MY_TEMPLATE_CONTEXT_PROCESSORS,
                'debug': MY_TEMPLATE_DEBUG,
                },
            },
        ]


#
# Migration locations
#

MIGRATION_MODULES = {
        ## django 1.8.2 not needed for cmsplugin-filer==1.1.3
        #'cmsplugin_filer_image': 'cmsplugin_filer_image.migrations_django',

        ## not needed for django 1.7.8 from v.3.0. Defaul migration folder is cms.migrations.
        #'cms': 'cms.migrations_django',
        #'menus': 'menus.migrations_django',

        ## Still using migrations_django for django 1.7 migrations.
        #'filer': 'filer.migrations_django',
        #'cmsplugin_filer_image': 'cmsplugin_filer_image.migrations_django',
        #'djangocms_text_ckeditor': 'djangocms_text_ckeditor.migrations_django',

        ## not checked. vim 0001* in app/migrations to see if it use django migration or South.
        # Add also the following modules if you're using these plugins:
        #'djangocms_file': 'djangocms_file.migrations_django',
        #'djangocms_flash': 'djangocms_flash.migrations_django',
        #'djangocms_googlemap': 'djangocms_googlemap.migrations_django',
        #'djangocms_inherit': 'djangocms_inherit.migrations_django',
        #'djangocms_link': 'djangocms_link.migrations_django',
    #'djangocms_picture': 'djangocms_picture.migrations_django',
    #'djangocms_snippet': 'djangocms_snippet.migrations_django',
    #'djangocms_teaser': 'djangocms_teaser.migrations_django',
    #'djangocms_video': 'djangocms_video.migrations_django',
    #'aldryn_news': 'aldryn_news.migrations_django',
    }

SOUTH_TESTS_MIGRATE = False

ROOT_URLCONF = 'vfoss_org.urls'

WSGI_APPLICATION = 'vfoss_org.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

#DATABASES = {
#    'default': {
#      #'ENGINE': 'django.db.backends.sqlite3',
#      #'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#      #'ENGINE': 'django.db.backends.mysql',
#      'ENGINE': 'django.db.backends.postgresql_psycopg2',
#      'NAME': 'vfosstestdb',
#      'USER': 'vfossdbuser',
#      'PASSWORD': 'vfossdbuser',
#      'HOST': '127.0.0.1',
#      'PORT': '5432',
#    }
#  }

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/
LANGUAGES = [
    ('en', 'English'),
    ('vi', 'Tieng Viet'),
]

LANGUAGE_CODE = 'en'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOCALE_PATHS = (
    os.path.join(BASE_DIR, "locale"),
)

# Site 
SITE_ID = 1

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATIC_URL = "/static/"

# django.contrib.staticfiles
STATICFILES_DIRS = (
    #os.path.join(PROJECT_DIR, 'static_dev'),
    os.path.join(BASE_DIR, 'themes'),
)

#For uploaded files, you will need to set up the MEDIA_ROOT setting:

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = "/media/"

## App specific

#
# Debug toolbar
#
INTERNAL_IPS = ('127.0.0.1',)
#
# Aldryn-news
#

#ALDRYN_NEWS_SEARCH = False

# http://django-haystack.readthedocs.org/en/latest/tutorial.html 
HAYSTACK_CONNECTIONS = {
  'default': {
    'ENGINE': 'haystack.backends.simple_backend.SimpleEngine',
  },
}

#
# django.contrib-admin
#

AUTH_USER_MODEL = 'auth.User'

#
# python-social-auth
#

# SOCIAL_AUTH_
AUTHENTICATION_BACKENDS = (
   #'social.backends.facebook.FacebookOAuth2',
   #'social.backends.google.GoogleOAuth2',
   #'social.backends.twitter.TwitterOAuth',
   'social_core.backends.open_id.OpenIdAuth',
   'social_core.backends.google.GoogleOpenId',
   'social_core.backends.google.GoogleOAuth2',
   'social_core.backends.google.GoogleOAuth',
   'social_core.backends.twitter.TwitterOAuth',
   'social_core.backends.yahoo.YahooOpenId',
   'django.contrib.auth.backends.ModelBackend',
   )

LOGIN_REDIRECT_URL = '/'

## test
#SOCIAL_AUTH_FACEBOOK_KEY = '1103359589683194'
#SOCIAL_AUTH_FACEBOOK_SECRET = '85efb1f63f487be9e38387d3c6d49469'
## production
SOCIAL_AUTH_FACEBOOK_KEY = get_secret("SOCIAL_AUTH_FACEBOOK_KEY")
SOCIAL_AUTH_FACEBOOK_SECRET = get_secret("SOCIAL_AUTH_FACEBOOK_SECRET")

SOCIAL_AUTH_POSTGRES_JSONFIELD = True

#
# Djangocms-blog
#
THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
    )
META_SITE_PROTOCOL = 'http'
META_USE_SITES = True

PARLER_LANGUAGES = {
    1: (
      {'code': 'en',},
      {'code': 'vi',},
      ),
    }

#
# django-analytical
#
CLICKY_SITE_ID = '100880251'




#
# Djangocms-xyz
#
