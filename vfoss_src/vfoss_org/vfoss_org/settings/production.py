import os
from os import environ, getenv
from .base import *

# Set variable to load dj_static in Production
PRODUCTION=True

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG=False

ALLOWED_HOSTS = [
    'vfoss.org',
    '*',
]

# SECURITY WARNING: keep the secret key used in production secret!
#SECRET_KEY = 'j@rxzf_*!7#f@i69g3sj0ca_zj8hnhu#ka^iw=cs+u$wuq$@z2'
SECRET_KEY = get_secret("SECRET_KEY")


COMPRESS_ENABLED = os.environ.get('COMPRESS_ENABLED', False) # needed when DEBUG=False. This causes 500 error.

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = ''
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False
EMAIL_PORT = 25
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER

# When 'NODB' is enabled,we skip Database and Cache setup. This is useful
# to test the rest of the Django deployment while boostrapping the application.
if os.getenv('NODB'):
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': 'db.sqlite3',
        }
    }
else:
    # Dockerfile reads the DB_PASSWORD from the secret into an environment
    # variable but its not there on kubectl exec. Soon Kubernetes versions
    # will have secrets as environment variables but currently just read it
    # from the volume
    DB_PASSWORD = os.getenv('DATABASE_PASSWORD')
    if not DB_PASSWORD:
        try:
            f = open('/etc/secrets/postgrespw')
            DB_PASSWORD = f.readline().rstrip()
        except IOError:
            pass
    if not DB_PASSWORD:
        raise Exception("No DJANGO_PASSWORD provided.")

    DATABASES = {
        'default': {
            'CONN_MAX_AGE': 0,
            'ENGINE': os.environ.get('DATABASE_ENGINE', 'django.db.backends.postgresql_psycopg2'), #'django.db.backends.postgresql_psycopg2',  # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': os.environ.get('DATABASE_NAME', 'vfossdb'),
            'USER': os.environ.get('DATABASE_USER', 'vfossdbuser'),
            'PASSWORD': DB_PASSWORD,
            'HOST': os.getenv('POSTGRES_SERVICE_HOST', '127.0.0.1'),
            'PORT': os.getenv('POSTGRES_SERVICE_PORT', 5432)
            #'HOST': os.environ.get('DATABASE_HOST', 'db'), # Compose's service name. Or.., Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
            #'PORT': os.environ.get('DATABASE_PORT', ''), #'PORT': '5432',                       # Set to empty string for default.
        }
    }

