"""
WSGI config for vfoss_org project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

#import os
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "vfoss_org.settings")
#
#from django.core.wsgi import get_wsgi_application
#application = get_wsgi_application()

import os
import sys
import site

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir('/var/www/vhosts/vfoss.org/vfossorg_env/lib/python2.7/site-packages')

# Add the app's directory to the PYTHONPATH
sys.path.append('/var/www/vhosts/vfoss.org/pydocs/vfossorg_pj/vfoss_org')
sys.path.append('/var/www/vhosts/vfoss.org/pydocs/vfossorg_pj/vfoss_org/vfoss_org')
#sys.path.append('/home/django_projects/MyProject/myproject')

os.environ['DJANGO_SETTINGS_MODULE'] = 'vfoss_org.settings'

# Activate your virtual env
activate_env=os.path.expanduser("/var/www/vhosts/vfoss.org/vfossorg_env/bin/activate_this.py")
execfile(activate_env, dict(__file__=activate_env))

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
#import django.core.handlers.wsgi
#application = django.core.handlers.wsgi.WSGIHandler()

