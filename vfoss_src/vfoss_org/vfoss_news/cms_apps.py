from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class VfossNewsApp(CMSApp):
    name = _("VFOSS News App")  # give your app a name, this is required
    urls = ["vfoss_news.urls"]  # link your app to url configuration(s)

    def get_urls(self, page=None, language=None, **kwargs):
        return ["vfoss_news.urls"]

apphook_pool.register(VfossNewsApp)  # register your app
