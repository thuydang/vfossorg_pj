from django.shortcuts import render
from django.http import HttpResponse

from vfoss_news.models import News

def index(request):
	# Construct a dictionary to pass to the template engine as its context.
	# Note the key boldmessage is the same as {{ boldmessage }} in the template!
	#context_dict = {'boldmessage': "I am bold font from the context"}

	# Query the database for a list of ALL categories currently stored.
	# Order the categories by no. likes in descending order.
	# Retrieve the top 5 only - or all if less than 5.
	# Place the list in our context_dict dictionary which will be passed to the template engine.
	#news_list = News.objects.order_by('-likes')[:5]
	#news_list = News.objects.order_by('-publication_start')[:5]
	news_list = News.objects.order_by('-publication_start')
	context_dict = {'object_list': news_list}

	# Render the response and send it back!
	return render(request, 'vfoss_news/news_list.html', context_dict)

