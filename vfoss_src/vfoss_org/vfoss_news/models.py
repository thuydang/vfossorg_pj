# -*- coding: utf-8 -*-
import datetime
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _, override
from django.utils.translation import get_language_from_request
from django.utils.timezone import now
from cms.utils.i18n import get_current_language
#from django.contrib.auth.models import User # using settings.AUTH_USER_MODEL 
from django.template.defaultfilters import slugify

from cms.models.fields import PlaceholderField
from cms.models.pluginmodel import CMSPlugin

from djangocms_text_ckeditor.fields import HTMLField

from hvad.models import TranslatableModel, TranslatedFields
from hvad.utils import get_translation
from unidecode import unidecode

from .managers import (
    RelatedManager,
    CategoryManager,
    )

def get_slug_in_language(record, language):
    if not record:
        return None
    if hasattr(record, record._meta.translations_cache) and language == record.language_code:  # possibly no need to hit db, try cache
        return record.lazy_translation_getter('slug')
    else:  # hit db
        try:
            translation = get_translation(record, language_code=language)
        except models.ObjectDoesNotExist:
            return None
        else:
            return translation.slug

class Category(TranslatableModel):
    """
    News category.
    """
    translations = TranslatedFields(
        name = models.CharField(_('Name'), max_length=255),
        slug = models.SlugField(_('Slug'),
            max_length=255, blank=True,
            help_text=_(
                'Auto-generated. Clean it to have it re-created. '
                'WARNING! Used in the URL. If changed, the URL will change. ')),
        meta = {'unique_together': [['slug']]}
    )

    #parent = models.ForeignKey('self', verbose_name=_('parent'), null=True, blank=True,)
    ordering = models.IntegerField(_('Ordering'), default=0)

    objects = CategoryManager()

    class Meta:
      verbose_name = _('News Category')
      verbose_name_plural = _('News Categories')
      ordering = ['ordering']

    def __unicode__(self):  #For Python 2, use __str__ on Python 3
      #return self.name # this works for multi-language
      return self.lazy_translation_getter('name', str(self.pk))


class News(TranslatableModel):
    """
    News
    """
    #THUMBNAIL_SIZE = getattr(settings, 'ALDRYN_NEWS_ITEM_THUMBNAIL_SIZE', '100x100')
    translations = TranslatedFields(
        title=models.CharField(_('Title'), max_length=255),
        slug=models.SlugField(
            _('Slug'),
            max_length=255,
            blank=True,
            help_text=_(
                'Auto-generated. Clean it to have it re-created. '
                'WARNING! Used in the URL. If changed, the URL will change. ')),
        lead_in=HTMLField(
            _('Lead-in'),
            help_text=_(
                'Will be displayed in lists, and at the start of the detail page')),
        meta={'unique_together': [['slug', 'language_code']]},
    )

    # PlaceholderField can not be in TranslatedFields. 
    # Be sure to combine both hvad’s TranslatableAdmin and PlaceholderAdminMixin 
    # when registering your model with the admin site
    content = PlaceholderField('news_content'),
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
            on_delete=models.PROTECT,
            blank=True, null=True)
    #key_visual = FilerImageField(verbose_name=_('Key Visual'), blank=True, null=True)
    publication_start = models.DateTimeField(
            _('Published Since'),
            default=datetime.datetime.now,
            help_text=_('Used in the URL. If changed, the URL will change.'))
    publication_end = models.DateTimeField(
            _('Published Until'), null=True, blank=True)
    category = models.ForeignKey(
            Category,
            on_delete=models.PROTECT,
            verbose_name=_('Category'),
            blank=True, null=True,
            help_text=_(
                'WARNING! Used in the URL. If changed, the URL will change.'))
    link = models.URLField(_('Link'),
            blank=True, null=True,
            help_text=_('This link will be used a absolute url'
            ))

    views = models.IntegerField(default=0)
    likes = models.IntegerField(default=0)

    objects = RelatedManager()

    class Meta:
        verbose_name = _('News')
        verbose_name_plural = _('News')
        ordering = ['-publication_start']

    def __unicode__(self):      #For Python 2, use __str__ on Python 3
        #return self.title
        return self.lazy_translation_getter('title', str(self.pk))

    def get_absolute_url(self, language=None):
        return self.link

## PluginModels
#class BaseNewsPlugin(CMSPlugin):
#  #news = models.ForeignKey(News)
#  abstract = True
#
#  def entry_queryset(self, request=None):
#    entries = News._default_manager.active_translations(language_code=language)
#    if not request or not getattr(request, 'toolbar', False) or not request.toolbar.edit_mode:
#      entries = entries.published()
#    return entries
#
#  def __str__(self):
#    return unicode(self.latest_posts)

class LatestNewsPlugin(CMSPlugin):
  """
  Plugin instance provide access to objects. Instance of this class is used
  by cms_plugin.xxxPublisher.render().
  """
  #latest_entries = models.IntegerField(_(u'News'), default=get_setting('LATEST_POSTS'),
  latest_entries = models.IntegerField(_(u'News'), default=5,
          help_text=_('The number of latests news to be displayed.'))

  def __unicode__(self):
    return str(self.latest_entries)

  #def get_entries(self, request):
  #  entries = self.entry_queryset(request)
  #  if self.tags.exists():
  #    #posts = posts.filter(tags__in=list(self.tags.all()))
  #    pass
  #  if self.categories.exists():
  #    #posts = posts.filter(categories__in=list(self.categories.all()))
  #    pass
  #  #user_language = self.request.GET.get('language')
  #  #return Mission.objects.language(user_language).all()
  #  return entries[:self.latest_posts]

  def get_news(self, request):
    #entries = News.objects.language(request.GET.get('language'))
    # Safer way to get language
    entries = News.objects.language(get_language_from_request(request, check_path=False))
    # Get all language
    #entries = News.objects.all()
    return entries[:self.latest_entries]


