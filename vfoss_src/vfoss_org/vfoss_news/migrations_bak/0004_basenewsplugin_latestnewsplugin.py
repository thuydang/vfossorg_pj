# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0011_auto_20150419_1006'),
        ('vfoss_news', '0003_auto_20150610_2216'),
    ]

    operations = [
        migrations.CreateModel(
            name='BaseNewsPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='LatestNewsPlugin',
            fields=[
                ('basenewsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='vfoss_news.BaseNewsPlugin')),
                ('latest_entries', models.IntegerField(default=5, help_text='The number of latests news to be displayed.', verbose_name='News')),
            ],
            options={
                'abstract': False,
            },
            bases=('vfoss_news.basenewsplugin',),
        ),
    ]
