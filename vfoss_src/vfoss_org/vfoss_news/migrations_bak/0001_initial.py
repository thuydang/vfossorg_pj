# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import djangocms_text_ckeditor.fields
from django.conf import settings
import cms.models.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('cms', '0011_auto_20150419_1006'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('slug', models.SlugField(help_text='Auto-generated. Clean it to have it re-created. WARNING! Used in the URL. If changed, the URL will change. ', max_length=255, verbose_name='Slug', blank=True)),
                ('ordering', models.IntegerField(default=0, verbose_name='Ordering')),
            ],
            options={
                'ordering': ['ordering'],
                'verbose_name': 'Category',
                'verbose_name_plural': 'Categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('slug', models.CharField(help_text='Auto-generated. Clean it to have it re-created. WARNING! Used in the URL. If changed, the URL will change. ', max_length=255, verbose_name='Slug', blank=True)),
                ('lead_in', djangocms_text_ckeditor.fields.HTMLField(help_text='Will be displayed in lists, and at the start of the detail page', verbose_name='Lead-in')),
                ('publication_start', models.DateTimeField(default=datetime.datetime.now, help_text='Used in the URL. If changed, the URL will change.', verbose_name='Published Since')),
                ('publication_end', models.DateTimeField(null=True, verbose_name='Published Until', blank=True)),
                ('views', models.IntegerField(default=0)),
                ('link', models.URLField(help_text='This link will be used a absolute url', null=True, verbose_name='Link', blank=True)),
                ('author', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('category', models.ForeignKey(blank=True, to='vfoss_news.Category', help_text='WARNING! Used in the URL. If changed, the URL will change.', null=True, verbose_name='Category')),
                ('content', cms.models.fields.PlaceholderField(slotname=b'news_content', editable=False, to='cms.Placeholder', null=True)),
            ],
            options={
                'ordering': ['-publication_start'],
                'verbose_name': 'News',
                'verbose_name_plural': 'News',
            },
            bases=(models.Model,),
        ),
    ]
