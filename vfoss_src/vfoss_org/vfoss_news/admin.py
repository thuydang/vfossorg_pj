# -*- coding: utf-8 -*-
from django.contrib import admin

from cms.admin.placeholderadmin import PlaceholderAdminMixin

from hvad.admin import TranslatableAdmin
from hvad.forms import translatable_inlineformset_factory

from .models import Category, News
from .forms import CategoryForm, NewsForm

# Register your models here.

# Add in this class to customized the Admin Interface
class NewsAdmin(TranslatableAdmin, PlaceholderAdminMixin, admin.ModelAdmin):

    class Meta:
        model = News

    form = NewsForm
    # [hvad] Notworking. prepopulated_fields = {"slug": ("title",)}

    list_display = ('__unicode__', 'all_translations')
    frontend_editable_fields = ('title', 'lead_in')

    # Override get_fieldsets to avoid admin checking for 
    # translated fields, which results in WrongManager error
    # django/contrib/admin/checks.py", line 176, in _check_field_spec_item
    def get_fieldsets(self, request, obj=None):
        fieldsets = (
                (None, {
                    'fields': (
                        'author', 'title', 'slug', 'link',
                        'publication_start',
                        'publication_end',
                        #, 'content', placeholderfield not working
                        'lead_in'
                        )
                    }),
                ('Advanced Settings', {
                    #'classes': ('collapse',),
                    'fields': (
                        'category',  'views', 'likes'
                        )
                    }),
                )
        return fieldsets

    # This hack is not working anymore:
    # https://github.com/KristianOellegaard/django-hvad/issues/98#issuecomment-17002604
    #
    #def __init__(self, *args, **kwargs):
    #    super(NewsAdmin, self).__init__(*args, **kwargs)
    #    self.prepopulated_fields = {'slug': ('title',)}

admin.site.register(News, NewsAdmin)


class CategoryAdmin(TranslatableAdmin):
    class Meta:
        model = Category

    form = CategoryForm #(initial={'headline': 'Initial headline'})
    # [hvad] Notworking. prepopulated_fields = {"slug": ("name",)}

    list_display = ('__unicode__', 'all_translations')

    # TODO: get_fieldsets has problem with translation
    # def get_fieldsets(self, request, obj=None):
    #     fieldsets = (
    #             (None, {
    #                 'fields': (
    #                     #'parent',
    #                     'name',
    #                     'slug'
    #                     )
    #                 })
    #             )
    #     return fieldsets

    # [hvad] Not working Workaround for prepopulated_fields and fieldsets from here:
    # https://github.com/KristianOellegaard/django-hvad/issues/10#issuecomment-5572524
    #def __init__(self, *args, **kwargs):
    #    super(CategoryAdmin, self).__init__(*args, **kwargs)
    #    self.prepopulated_fields = {'slug': ('name',)}

admin.site.register(Category, CategoryAdmin)


