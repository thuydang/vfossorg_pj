from django.db import models
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from django.utils.translation import ugettext as _
from django.utils.translation import get_language_from_request

from .models import News, LatestNewsPlugin
from .settings import get_setting

# Plugin class
class NewsPluginPublisher(CMSPluginBase):
  module = _("News")

@plugin_pool.register_plugin
class LatestNewsPluginPublisher(NewsPluginPublisher):
  model = LatestNewsPlugin
  name = _("VFOSS Latest News Plugin")
  render_template = "vfoss_news/plugins/latest_entries.html"

  def render(self, context, instance, placeholder):
    context.update({'instance': instance})
    #context.update['instance'] = instance
    context['news_list'] = instance.get_news(context['request'])
    #get_language_from_request(request, check_path=False))
    #context['TRUNCWORDS_COUNT'] = get_setting('POSTS_LIST_TRUNCWORDS_COUNT')
    return context

# If @plugin_pool.register_plugin annotation is not used.
#plugin_pool.register_plugin(NewsLatestEntriesPlugin)  # register the plugin
