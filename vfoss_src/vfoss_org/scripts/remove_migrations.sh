#!/bin/sh
echo "Starting ..."

echo ">> Deleting old migrations from INPUT PATH $1"
find $1 -path "*/migrations/*.py" -not -name "__init__.py" -delete

# Optional
echo ">> Deleting database"
find . -name "db.sqlite3" -delete

echo ">> Please, Running manage.py makemigrations"
#python manage.py makemigrations

echo ">> Please, Running manage.py migrate"
#python manage.py migrate

echo ">> Done"
