import sys

SECRET_KEY = 'fake-key'
INSTALLED_APPS = [
    "tests",
    #
    'django.contrib.auth',
    'people',
]


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d (thread)d %(message)s'
        },
        'simple':{
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        #'null': {
        #    'level': 'DEBUG',
        #    'class': 'django.utils.log.NullHandler',
        #},
        'console': {
            'level':'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console', ],
            'propagate': True,
            'level': 'DEBUG'

        },
        'payments_system': {
            'handlers': ['console', ],
            'propagate': True,
            'level': 'DEBUG'
        }
    }
}
