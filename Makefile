# defaul shell
SHELL = /bin/bash

# Rule "help"
.PHONY: help
.SILENT: help
help:
	echo "Use make [rule]"
	echo "Rules:"
	echo ""
	echo "build 		- build application and generate docker image"
	echo "run-db 		- run mysql database on docker"
	echo "run-app		- run application on docker"
	echo "stop-app	- stop application"
	echo "stop-db		- stop database"
	echo "rm-app		- stop and delete application"
	echo "rm-db		- stop and delete database"
	echo ""
	echo "k-setup		- init minikube machine"
	echo "k-deploy-db	- deploy mysql on cluster"
	echo "k-build-app	- build app and create docker image inside minikube"
	echo "k-deploy-app	- deploy app on cluster"
	echo ""
	echo "k-start		- start minikube machine"
	echo "k-all		- do all the above k- steps"
	echo "k-stop		- stop minikube machine"
	echo "k-delete	- stop and delete minikube machine"
	echo ""
	echo "check		- check tools versions"
	echo "help		- show this message"

build: build-app build-db

build-app:
	#mvn clean install; \
	
	cd vfoss_src; \
	docker build --force-rm -t vfoss_org_web_k8s .

build-db:
	#mvn clean install; \
	
	cd postgres; \
	docker build --force-rm -t vfoss_org_db_k8s .



run-db: stop-db rm-db
	docker run --name mysql57 -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 -e MYSQL_USER=java -e MYSQL_PASSWORD=1234 -e MYSQL_DATABASE=k8s_java -d mysql/mysql-server:5.7

run-app: stop-app rm-app
	docker run --name vfoss-org-app -p 8080:8080 -d -e DATABASE_SERVER_NAME=mysql57 --link mysql57:mysql57 vfoss_org_web_k8s:latest

stop-app:
	docker stop vfoss-org-app; \

stop-db:
	docker stop mysql57

rm-app:	stop-app
	docker rm vfoss-org-app; \

rm-db: stop-db
	docker rm mysql57

k-setup:
	rm  ~/.config/VirtualBox/HostInterfaceNetworking-vboxnet0-Dhcpd.leases; \
	rm  ~/.config/VirtualBox/HostInterfaceNetworking-vboxnet0-Dhcpd.leases-prev; \
	minikube -p vfoss.kube start --cpus 2 --memory=4098; \
	minikube -p vfoss.kube addons enable ingress; \
	minikube -p vfoss.kube addons enable metrics-server; \
	kubectl create namespace vfoss

k-deploy-db:
	kubectl apply -f kubernetes/mysql/;

k-build-app:
	#mvn clean install; \

k-build-image:
	eval $$(minikube -p vfoss.kube docker-env) && cd vfoss_src && docker build --force-rm -t vfoss_org_web_k8s .;

k-deploy-app:
	kubectl apply -n vfoss -f kubernetes/django/;

k-start:
	minikube -p vfoss.kube start;

k-all: k-setup k-deploy-db k-build-image k-deploy-app

k-stop:
	minikube -p vfoss.kube stop;

k-delete:
	minikube -p vfoss.kube stop && minikube -p vfoss.kube delete

check:
	echo "make version " && make --version && echo
	minikube version && echo
	echo "kubectl version" && kubectl version --short --client && echo
	echo "virtualbox version" && vboxmanage --version  && echo

