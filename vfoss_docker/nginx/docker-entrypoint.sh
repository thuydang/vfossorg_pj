#!/bin/bash
set -e

# get gw IP which is..
export DOCKER_HOST_IP=$(ip route show 0.0.0.0/0 | grep -Eo 'via \S+' | awk '{ print $2 }')

sed '1 i\
env DOCKER_HOST_IP;' /etc/nginx/nginx.conf

exec nginx -g 'daemon off;' "$@"
#exec "$@"
