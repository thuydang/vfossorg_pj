#!/usr/bin/env bash

# create the dbdata volume
docker volume create --name=anthenao_db_data

# start the applications on the backend network
cd ./eco/backend/ && docker-compose --x-networking up 
