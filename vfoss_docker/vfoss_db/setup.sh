#!/usr/bin/env bash

set -e

echo "----->  this is it pg 9.5"


export DB_NAME=vfossdb
export DB_USER=vfossdbuser

# $POSTGRES_USER is postgres by default
psql -v ON_ERROR_STOP=1 --username="$POSTGRES_USER" <<-EOSQL
    CREATE USER $DB_USER;
		CREATE DATABASE $DB_NAME;
		GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO $DB_USER;
		EOSQL

#createuser -U postgres --createdb --createrole $POSTGRES_USER;  
#createdb -U $POSTGRES_USER $POSTGRES_DB;
